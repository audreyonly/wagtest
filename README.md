
# Wag Project
Get and Display Stackoverflow users.

## Purpose
It shows the use of **clean MVP architecture** to display Stackoverflow users cards fetched from [Stackoverflow Users API]([https://api.stackexchange.com/docs](https://api.stackexchange.com/docs)) .
It also caches the response fetched in a database for offline usage. This mechanism is handled by a repository layer.

## Features
The user cards will be fetched using a network call and displayed in a RecyclerView. Each card shows the user's name, badges and gravatar that will be downloaded only once and store on the disk for any subsequent requests. Additionally, It integrates a pull to refresh functionality so that the user can force refresh the data from the internet. Once the user clicks on a card it will display the user profile information in a Web Browser.

## Android Libraries used

I used some useful android libraries to make the code clean, elegant, maintainable, scalable and make a clear separation of responsibilities.
Here the libraries that has been used :

- **Retrofit** : Handles Network calls. It simplifies the integration of REST based architecture using HTTP based annotations. It makes easy to retrieve data from the internet and using the converters it deserialized the response in Java model in a elegant manner. I also allows to transform the retrofit response into Rx Observable using an adapter.
- **Room** Handles Database access. I chose this library because it provides an abstraction layer over SQLite. Using annotation I was able to create a content provider quickly. Without this library, I would have handled all the CRUD operations to access a SQLite DB, the contract classes, and perhaps used a CursorLoader to fetch the data asynchronously. Thanks to Room, I used DAO to handle the CRUD operations which is cleaner, easier to understand and to unit tests.
- **RxJava** Asynchronous data stream processing and to benefit the advantages of Observer Pattern. Using RxJava, I observed the data stream from the presenter and notifies the UI accordingly. It simplifies asynchronous task based on the fact that we are able to choose on which Schedulers we want to observe stream. It raises the level of abstraction around threading.
- **Dagger 2.0** Dependency Injection Framework. As we were thinking about reusability, scalability, maintainability and single responsibility, I used Dagger to decouple the creation of objects from there usage. We can just replace and make the code more readable and more testable. We can replace the injected dependency with a Mock during UI or Unit testing.
- **Glide** for asynchronous images downloading and caching. GLide simplify the image fetching and has a powerful caching mechanism. I chose GLide to load gravatars using their url and cache them on the disk to avoid having to download the image more than once.
- **Gson** to easily transform POJO <-->JSON. To avoid having the boilerplate of Json serialization/deserialization process. Based on annotation in my Item class, it will transform the Json payload into Java model.
- **Butterknife** Android View injection - Using this, helps me to write common boilerplate view code like findViewById, onClickListener and more others based on annotations which make the code cleaner by significantly reducing the lines of boilerplate code written.

## Setup

### Clone the Repository

As usual, get started by cloning the project on your local machine:  
```
$ git clone https://audreyange93@bitbucket.org/audreyonly/wagtest.git
```
You can also create a new project from Android Studio by importing this project from git using the above git repository url.

### Prerequisites

This project uses JDK 8 and has been built using android SDK 28. It also uses the [androidx.*]([https://developer.android.com/jetpack/androidx](https://developer.android.com/jetpack/androidx)) package libraries.

## Authors

- **Audrey Maumoin**
Android Developer

## Acknowledgments
I hope that you will have a good time navigating through the project.

**Happy Reading :smiley:**