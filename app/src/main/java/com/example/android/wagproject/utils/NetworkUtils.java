package com.example.android.wagproject.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.example.android.wagproject.di.annotation.AppContext;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public final class NetworkUtils {

  private final Context applicationContext;

  @Inject
  public NetworkUtils(@AppContext Context applicationContext) {
    this.applicationContext = applicationContext;
  }

  /**
   * Returns Whether the network is available.
   */
  public boolean isNetworkAvailable() {
    ConnectivityManager cm =
        (ConnectivityManager) applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    return activeNetwork != null &&
        activeNetwork.isConnected();
  }
}
