package com.example.android.wagproject.repo.rest;

import com.example.android.wagproject.model.ItemWrapper;
import io.reactivex.Single;
import java.util.Map;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ItemApi {

  @GET("users")
  Single<ItemWrapper> getItemList(@QueryMap Map<String, String> params);
}
