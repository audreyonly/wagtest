package com.example.android.wagproject.repo.rest;

import com.example.android.wagproject.repo.error.ItemError;
import com.example.android.wagproject.repo.error.ItemError.ErrorType;
import com.example.android.wagproject.utils.NetworkUtils;
import java.io.IOException;
import javax.inject.Inject;
import javax.inject.Singleton;
import okhttp3.Interceptor;
import okhttp3.Response;

@Singleton
public class NetworkInterceptor implements Interceptor {

  private NetworkUtils networkUtils;

  @Inject
  public NetworkInterceptor(NetworkUtils networkUtils) {
    this.networkUtils = networkUtils;
  }


  @Override
  public Response intercept(Chain chain) throws IOException {
    if (!networkUtils.isNetworkAvailable()) {
      throw new ItemError(ErrorType.NO_INTERNET);
    }
    return chain.proceed(chain.request());
  }
}
