package com.example.android.wagproject.repo.rest;

import javax.inject.Singleton;
import retrofit2.Retrofit;

@Singleton
public class RestClient {

  private final Retrofit retrofit;

  public RestClient(Retrofit retrofit) {
    this.retrofit = retrofit;
  }

  public ItemApi getItemApi(Class<ItemApi> apiClass) {
    return retrofit.create(apiClass);
  }

}
