package com.example.android.wagproject.model;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "item")
public class Item implements Comparable<Item> {

  @SerializedName("account_id")
  @PrimaryKey
  private int accountId;
  @SerializedName("reputation")
  private int reputation;
  @SerializedName("badge_counts")
  @Embedded(prefix = "badge")
  private Badge badge;
  @SerializedName("location")
  private String location;
  @SerializedName("website_url")
  private String websiteUrl;
  @SerializedName("link")
  private String link;
  @SerializedName("profile_image")
  private String profileImageUrl;
  @SerializedName("display_name")
  private String name;

  public Badge getBadge() {
    return badge;
  }

  public void setBadge(Badge badge) {
    this.badge = badge;
  }

  public int getReputation() {
    return reputation;
  }

  public void setReputation(int reputation) {
    this.reputation = reputation;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getProfileImageUrl() {
    return profileImageUrl;
  }

  public void setProfileImageUrl(String profileImageUrl) {
    this.profileImageUrl = profileImageUrl;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  public int getAccountId() {
    return accountId;
  }

  public void setAccountId(int accountId) {
    this.accountId = accountId;
  }

  public String getWebsiteUrl() {
    return websiteUrl;
  }

  public void setWebsiteUrl(String websiteUrl) {
    this.websiteUrl = websiteUrl;
  }

  @Override
  public int compareTo(Item o) {
    return accountId - o.accountId;
  }
}
