package com.example.android.wagproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.android.wagproject.R;
import com.example.android.wagproject.adapter.ItemAdapter.ItemViewHolder;
import com.example.android.wagproject.model.Item;
import com.example.android.wagproject.utils.ListUtils;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class ItemAdapter extends Adapter<ItemViewHolder> {

  private List<Item> itemList;
  private Context context;
  private ItemClickListener listener;
  private int gravatarSize;

  public ItemAdapter(Context context, List<Item> items) {
    this.context = context;
    this.itemList = items;
    this.gravatarSize = context.getResources().getDimensionPixelSize(R.dimen.gravatar_size);
  }

  public void addList(List<Item> items) {
    itemList.addAll(items);
    notifyDataSetChanged();
  }

  public void setOnItemClickListener(ItemClickListener listener) {
    this.listener = listener;
  }

  @NonNull
  @Override
  public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    View view = LayoutInflater.from(context).inflate(R.layout.item_layout, viewGroup, false);
    return new ItemViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull ItemViewHolder itemViewHolder, int position) {
    Item item = itemList.get(position);
    itemViewHolder.usernameView.setText(item.getName());
    itemViewHolder.locationView.setText(item.getLocation());
    itemViewHolder.reputationView.setText(getFormattedNumber(item.getReputation()));
    itemViewHolder.badgeBronzeView.setText(getFormattedNumber(item.getBadge().getBronzeCount()));
    itemViewHolder.badgeSilverView.setText(getFormattedNumber(item.getBadge().getSilverCount()));
    itemViewHolder.badgeGoldView.setText(getFormattedNumber(item.getBadge().getGoldCount()));
    // The image will be downloaded only once, store on the disk and the file Uri will be used to
    // fetch the image after.
    Glide.with(context).load(item.getProfileImageUrl())
        .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .override(gravatarSize, gravatarSize))
        .into(itemViewHolder.profilePictureView);
    itemViewHolder.container.setOnClickListener(v -> listener.onItemClick(item.getLink()));
  }

  private String getFormattedNumber(int number) {
    return NumberFormat.getNumberInstance(Locale.getDefault()).format(number);
  }

  @Override
  public int getItemCount() {
    return ListUtils.size(itemList);
  }

  /**
   * Listener that notifies when a click occurs on a item.
   */
  public interface ItemClickListener {

    /**
     * Notifies the fragment when click on item.
     */
    void onItemClick(String url);
  }

  static class ItemViewHolder extends ViewHolder {

    @BindView(R.id.item_container)
    ConstraintLayout container;
    @BindView(R.id.profile_picture_view)
    ImageView profilePictureView;
    @BindView(R.id.username_view)
    TextView usernameView;
    @BindView(R.id.location_view)
    TextView locationView;
    @BindView(R.id.reputation_view)
    TextView reputationView;
    @BindView(R.id.badge_bronze_view)
    TextView badgeBronzeView;
    @BindView(R.id.badge_silver_view)
    TextView badgeSilverView;
    @BindView(R.id.badge_gold_view)
    TextView badgeGoldView;

    ItemViewHolder(@NonNull View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
