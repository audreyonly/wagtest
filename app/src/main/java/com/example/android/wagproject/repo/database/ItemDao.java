package com.example.android.wagproject.repo.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import com.example.android.wagproject.model.Item;
import io.reactivex.Completable;
import io.reactivex.Single;
import java.util.List;

@Dao
public interface ItemDao {

  @Query("SELECT * FROM item ORDER BY accountId ASC")
  Single<List<Item>> getAllItems();

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  Completable insertAllItems(List<Item> items);
}
