package com.example.android.wagproject.di;

import android.app.Application;
import android.content.Context;
import com.example.android.wagproject.di.annotation.AppContext;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@Module
public class AppModule {

  private Application application;

  public AppModule(Application application) {
    this.application = application;
  }

  @Provides
  @Singleton
  @AppContext
  public Context provideApplicationContext() {
    return application;
  }

  @Provides
  @Singleton
  public Application provideApplication() {
    return application;
  }
}
