package com.example.android.wagproject.utils;

import java.util.List;

public class ListUtils {

  private ListUtils() {
    // Shall not be constructed.
  }

  /**
   * Returns {@code true} when the list is null or empty;
   */
  public static boolean isEmpty(List<?> list) {
    return size(list) == 0;
  }

  /**
   * Return the list size.
   */
  public static int size(List<?> list) {
    return list != null ? list.size() : 0;
  }
}
