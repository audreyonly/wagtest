package com.example.android.wagproject.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.example.android.wagproject.R;
import com.example.android.wagproject.adapter.ItemAdapter;
import com.example.android.wagproject.adapter.ItemAdapter.ItemClickListener;
import com.example.android.wagproject.di.annotation.Scope.FragmentScope;
import com.example.android.wagproject.fragment.ItemListPresenter.ItemListView;
import com.example.android.wagproject.model.Item;
import com.example.android.wagproject.utils.ListUtils;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class ItemListFragment extends Fragment implements ItemListView, ItemClickListener,
    OnRefreshListener {

  @BindView(R.id.recycler_view)
  RecyclerView recyclerView;
  @BindView(R.id.empty_view)
  TextView emptyView;
  @BindView(R.id.swipe_refresh_layout)
  SwipeRefreshLayout swipeRefreshLayout;
  @Inject
  ItemListPresenter presenter;
  private ItemAdapter adapter;
  private Unbinder unbinder;

  @Override
  public void onAttach(@NonNull Context context) {
    AndroidSupportInjection.inject(this);
    super.onAttach(context);
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    // Retain this fragment across configuration changes.
    setRetainInstance(true);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View view = inflater.inflate(R.layout.item_list_fragment_layout, container, false);
    unbinder = ButterKnife.bind(this, view);

    swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
    swipeRefreshLayout.setOnRefreshListener(this);
    GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),
        getResources().getInteger(R.integer.item_span_count));
    adapter = new ItemAdapter(getActivity(), new ArrayList<>());
    adapter.setOnItemClickListener(this);
    recyclerView.setLayoutManager(layoutManager);
    recyclerView.setAdapter(adapter);
    presenter.fetchItemList(savedInstanceState == null);
    return view;
  }

  @Override
  public void displayItemList(List<Item> items) {
    if (!ListUtils.isEmpty(items)) {
      adapter.addList(items);
      setEmptyViewVisibility(false);
    } else {
      setEmptyViewVisibility(true);
      emptyView.setText(R.string.no_data);
    }
  }

  private void setEmptyViewVisibility(boolean isVisible) {
    if (isVisible) {
      recyclerView.setVisibility(View.GONE);
      emptyView.setVisibility(View.VISIBLE);
    } else {
      recyclerView.setVisibility(View.VISIBLE);
      emptyView.setVisibility(View.GONE);
    }
  }

  @Override
  public void showErrorToast() {
    Toast.makeText(getActivity(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT)
        .show();
  }

  @Override
  public void showNoConnectionError() {
    setEmptyViewVisibility(true);
    emptyView.setText(R.string.no_internet_connection);
  }

  @Override
  public void setRefreshingState(boolean state) {
    swipeRefreshLayout.setRefreshing(state);
  }

  @Override
  public void onItemClick(String url) {
    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
      startActivity(intent);
    }
  }

  @Override
  public void onRefresh() {
    presenter.refreshItemList();
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    presenter.onDestroy();
  }

  @Module
  public abstract static class ItemListFragmentModule {

    @ContributesAndroidInjector(modules = {PresenterProviderModule.class})
    @FragmentScope
    abstract ItemListFragment contributeItemListFragmentInjector();

  }

  @Module
  public static class PresenterProviderModule {

    @Provides
    @FragmentScope
    public ItemListView provideItemListView(ItemListFragment fragment) {
      return fragment;
    }
  }
}
