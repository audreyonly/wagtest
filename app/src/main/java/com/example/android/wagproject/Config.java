package com.example.android.wagproject;

public final class Config {

  public static final String BASE_URL = "https://api.stackexchange.com/2.2/";
  public static final String STACKOVERFLOW = "stackoverflow";
  public static final int READ_TIME_OUT = 10000;
  public static final int CONNECT_TIME_OUT = 15000;
}
