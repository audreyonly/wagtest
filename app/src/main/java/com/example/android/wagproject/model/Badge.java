package com.example.android.wagproject.model;

import com.google.gson.annotations.SerializedName;

public class Badge {

  @SerializedName("bronze")
  private int bronzeCount;
  @SerializedName("silver")
  private int silverCount;
  @SerializedName("gold")
  private int goldCount;

  public int getBronzeCount() {
    return bronzeCount;
  }

  public void setBronzeCount(int bronzeCount) {
    this.bronzeCount = bronzeCount;
  }

  public int getSilverCount() {
    return silverCount;
  }

  public void setSilverCount(int silverCount) {
    this.silverCount = silverCount;
  }

  public int getGoldCount() {
    return goldCount;
  }

  public void setGoldCount(int goldCount) {
    this.goldCount = goldCount;
  }
}
