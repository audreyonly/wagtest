package com.example.android.wagproject;

import android.app.Activity;
import android.app.Application;
import com.example.android.wagproject.di.AppModule;
import com.example.android.wagproject.di.DaggerWagComponent;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import javax.inject.Inject;

public class WagApplication extends Application implements HasActivityInjector {

  @Inject
  DispatchingAndroidInjector<Activity> dispatchingActivityInjector;

  @Override
  public void onCreate() {
    super.onCreate();
    DaggerWagComponent.builder().appModule(new AppModule(this)).build().inject(this);
  }

  @Override
  public AndroidInjector<Activity> activityInjector() {
    return dispatchingActivityInjector;
  }
}
