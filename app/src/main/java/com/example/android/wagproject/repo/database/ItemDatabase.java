package com.example.android.wagproject.repo.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import com.example.android.wagproject.model.Item;

@Database(entities = {Item.class}, version = 1, exportSchema = false)
public abstract class ItemDatabase extends RoomDatabase {

  public static final String DB_NAME = "item_db";

  public abstract ItemDao daoAccess();
}
