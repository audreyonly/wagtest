package com.example.android.wagproject.repo.error;

import java.io.IOException;

/**
 * Decorator class for Exception raised when fetching {@link com.example.android.wagproject.model.Item}
 * data.
 */
public class ItemError extends IOException {

  private ErrorType errorType;

  public ItemError() {
    this(ErrorType.DEFAULT);
  }

  public ItemError(ErrorType errorType) {
    super(errorType.message);
    this.errorType = errorType;
  }

  /**
   * Convert the Throwable instance to ItemError.
   */
  public static ItemError fromThrowable(Throwable throwable) {
    if (throwable instanceof ItemError) {
      return (ItemError) throwable;
    } else {
      return new ItemError();
    }
  }

  public ErrorType getErrorType() {
    return errorType;
  }

  public enum ErrorType {
    NO_INTERNET("No Internet Connection"),
    DEFAULT("Default");

    private String message;

    ErrorType(String message) {
      this.message = message;
    }
  }
}
