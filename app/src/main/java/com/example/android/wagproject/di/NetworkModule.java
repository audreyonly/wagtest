package com.example.android.wagproject.di;

import com.example.android.wagproject.Config;
import com.example.android.wagproject.repo.rest.NetworkInterceptor;
import com.example.android.wagproject.repo.rest.RestClient;
import dagger.Module;
import dagger.Provides;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

  @Provides
  @Singleton
  public HttpLoggingInterceptor provideInterceptor() {
    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(Level.BASIC);
    return interceptor;
  }

  @Provides
  @Singleton
  public OkHttpClient provideOkHttpClient(HttpLoggingInterceptor interceptor, NetworkInterceptor networkInterceptor) {
    OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder()
        .connectTimeout(Config.CONNECT_TIME_OUT, TimeUnit.SECONDS)
        .readTimeout(Config.READ_TIME_OUT, TimeUnit.SECONDS)
        .writeTimeout(Config.READ_TIME_OUT, TimeUnit.SECONDS);
    httpClient.addInterceptor(interceptor);
    httpClient.addInterceptor(networkInterceptor);
    return httpClient.build();
  }

  @Provides
  @Singleton
  public Retrofit provideRetrofit(OkHttpClient httpClient) {
    return new Retrofit.Builder()
        .baseUrl(Config.BASE_URL)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(
            GsonConverterFactory.create())
        .client(httpClient)
        .build();
  }

  @Provides
  @Singleton
  public RestClient provideRestClient(Retrofit retrofit) {
    return new RestClient(retrofit);
  }

}
