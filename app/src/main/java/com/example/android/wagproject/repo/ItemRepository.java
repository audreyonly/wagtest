package com.example.android.wagproject.repo;

import static com.example.android.wagproject.Config.STACKOVERFLOW;

import android.content.Context;
import android.util.Log;
import androidx.room.Room;
import com.example.android.wagproject.di.annotation.AppContext;
import com.example.android.wagproject.model.Item;
import com.example.android.wagproject.repo.database.ItemDao;
import com.example.android.wagproject.repo.database.ItemDatabase;
import com.example.android.wagproject.repo.error.ItemError;
import com.example.android.wagproject.repo.error.ItemError.ErrorType;
import com.example.android.wagproject.repo.rest.ItemApi;
import com.example.android.wagproject.repo.rest.RestClient;
import com.example.android.wagproject.utils.ListUtils;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ItemRepository {

  private static final String TAG = "ItemRepository";

  private RestClient restClient;
  private ItemDao itemDao;
  private List<Item> itemList;

  @Inject
  public ItemRepository(@AppContext Context appContext, RestClient restClient) {
    this.restClient = restClient;
    ItemDatabase database = Room.databaseBuilder(appContext,
        ItemDatabase.class, ItemDatabase.DB_NAME).build();
    itemDao = database.daoAccess();

  }

  /**
   * Fetch the items list. This load the items from internet, memory cache or local DB. It will
   * first load from the internet. If the load is successful, the items will be store in memory and
   * into a DB for further usage. The subsequent calls will be loaded from the the memory cache
   * unless the cache is empty or voluntary skipped.  If the cache is empty or skipped and for any
   * reasons, the data can't be loaded from the internet, it will fallback to the DB.
   *
   * @param skipCache Whether to skip the memory cache.
   */
  public Single<List<Item>> getItemList(boolean skipCache) {
    if (skipCache || ListUtils.isEmpty(itemList)) {
      ItemApi itemApi = restClient.getItemApi(ItemApi.class);
      return itemApi.getItemList(getQueryParams())
          .flatMap(response -> Single.just(response.getItems()))
          .doOnSuccess(this::cacheItemResponse).onErrorResumeNext(this::fetchFromCache);
    } else {
      return Single.just(itemList);
    }
  }

  private void insertItems(List<Item> items) {
    itemDao.insertAllItems(items).subscribeOn(Schedulers.io())
        .subscribe(() -> Log.d(TAG, "items inserted with success"),
            throwable -> Log.d(TAG, "items insertion failed", throwable)
        );
  }

  private Map<String, String> getQueryParams() {
    HashMap<String, String> params = new HashMap<>();
    params.put("site", STACKOVERFLOW);
    return params;
  }

  private void cacheItemResponse(List<Item> items) {
    // Clear the cache to ensure that we have the most updated response.
    clearItemsCache();
    // Cache items in memory.
    itemList = items;
    // Cache items in DB.
    insertItems(itemList);
  }

  private Single<List<Item>> fetchFromCache(Throwable throwable) {
    if (!ListUtils.isEmpty(itemList)) {
      return Single.error(new ItemError());
    }
    return fetchFromDB(throwable);
  }

  private Single<List<Item>> fetchFromDB(Throwable throwable) {
    return itemDao.getAllItems().flatMap(items -> {
      ItemError itemError = ItemError.fromThrowable(throwable);
      if (ListUtils.isEmpty(items) && ErrorType.NO_INTERNET.equals(itemError.getErrorType())) {
        return Single.error(itemError);
      }
      itemList = items;
      return Single.just(items);
    });
  }

  /**
   * Clear the items cached into the memory.
   */
  public void clearItemsCache() {
    if (!ListUtils.isEmpty(itemList)) {
      itemList.clear();
    }
  }
}
