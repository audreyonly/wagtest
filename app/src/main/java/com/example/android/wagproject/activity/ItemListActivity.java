package com.example.android.wagproject.activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import com.example.android.wagproject.R;
import com.example.android.wagproject.di.annotation.Scope.ActivityScope;
import com.example.android.wagproject.fragment.ItemListFragment;
import com.example.android.wagproject.fragment.ItemListFragment.ItemListFragmentModule;
import dagger.Module;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.ContributesAndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import javax.inject.Inject;

public class ItemListActivity extends AppCompatActivity implements HasSupportFragmentInjector {

  @Inject
  DispatchingAndroidInjector<Fragment> fragmentInjector;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    AndroidInjection.inject(this);
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    if (savedInstanceState == null) {
      FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
      transaction.replace(R.id.fragment_container, new ItemListFragment()).commit();
    }
  }

  @Override
  public AndroidInjector<Fragment> supportFragmentInjector() {
    return fragmentInjector;
  }

  @Module
  public abstract static class ItemListActivityModule {

    @ContributesAndroidInjector(modules = ItemListFragmentModule.class)
    @ActivityScope
    abstract ItemListActivity contributeItemListActivityInjector();
  }
}
