package com.example.android.wagproject.fragment;

import android.util.Log;
import com.example.android.wagproject.model.Item;
import com.example.android.wagproject.repo.ItemRepository;
import com.example.android.wagproject.repo.error.ItemError;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;

public class ItemListPresenter {

  private final CompositeDisposable compositeDisposable;
  private ItemListView itemListView;
  private ItemRepository repository;
  private final Consumer<Throwable> loadingError = (Throwable throwable) -> {
    itemListView.setRefreshingState(false);
    Log.e(this.toString(), "error happens ->" + throwable.getLocalizedMessage());
    if (throwable instanceof ItemError) {
      switch (((ItemError) throwable).getErrorType()) {
        case NO_INTERNET:
          itemListView.showNoConnectionError();
          break;
        case DEFAULT:
          itemListView.showErrorToast();
          break;
      }
    }
  };

  @Inject
  public ItemListPresenter(ItemListView itemListView, ItemRepository repository) {
    this.itemListView = itemListView;
    this.repository = repository;
    compositeDisposable = new CompositeDisposable();
  }

  public void refreshItemList() {
    fetchItemList(true);
  }

  public void fetchItemList(boolean skipCache) {
    itemListView.setRefreshingState(true);
    compositeDisposable.add(repository.getItemList(skipCache)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread()).subscribe(items -> {
          Collections.sort(items);
          itemListView.setRefreshingState(false);
          itemListView.displayItemList(items);
        }, loadingError));
  }

  public void onDestroy() {
    // Avoid memory leaks
    compositeDisposable.clear();
    repository.clearItemsCache();
    itemListView = null;
  }

  interface ItemListView {

    void displayItemList(List<Item> items);

    void showErrorToast();

    void showNoConnectionError();

    void setRefreshingState(boolean state);
  }

}
