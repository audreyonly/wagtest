package com.example.android.wagproject.model;

import com.google.gson.annotations.SerializedName;
import java.util.Collections;
import java.util.List;

public class ItemWrapper {

  @SerializedName("items")
  private List<Item> items;

  public List<Item> getItems() {
    return items != null ? items : Collections.emptyList();
  }
}
