package com.example.android.wagproject.di;

import com.example.android.wagproject.WagApplication;
import com.example.android.wagproject.activity.ItemListActivity.ItemListActivityModule;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import javax.inject.Singleton;

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class, ItemListActivityModule.class,
    AppModule.class,
    NetworkModule.class})
public interface WagComponent extends AndroidInjector<WagApplication> {

}
